from django.shortcuts import render, redirect
from .forms import RegistrationForm, LoginForm, EditProfileForm
from django.contrib.auth.decorators import login_required
from accounts.models import UserProfile
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from orders import views
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.forms import UserChangeForm

def check_admin(user):
	return user.is_superuser

@user_passes_test(check_admin)
def register(request):
	if request.method == 'POST':
		form = RegistrationForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/order')

	else:
		form = RegistrationForm()

		args = {'form' : form}
		return render(request, 'accounts/reg_form.html', args)

def view_profile(request):
	args = {'user' : request.user}
	return render(request, 'accounts/profile.html', args)

@user_passes_test(check_admin)
def allusers(request):
	users = UserProfile.objects.all()
	args = {'users': users}
	return render(request, 'accounts/allusers.html', args)

@user_passes_test(check_admin)
def edit_profile(request, pk):
	k = UserProfile.objects.get(pk=pk)
	if request.method == 'POST':
		form = EditProfileForm(request.POST, instance=k.user)
		if form.is_valid():
			#cno = form.cleaned_data['contact_number']
			bamt = form.cleaned_data['balance_amount']
			form.save()
			#k.contact_number = cno
			k.balance_amount += bamt
			k.save()
			return redirect('/account/allusers')

	else:
		form = EditProfileForm(instance=k.user)
		args = {'form':form, 'user':k}
		return render(request, 'accounts/edit_profile.html', args)

def login_view(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			#access = form.cleaned_data['access']
			user = authenticate(request, username=username, password=password)
			#if (access == ''):
			#	print ("cannot get access value")
			if user is not None:
				login(request, user)
				k = UserProfile.objects.get(user=user)
				access = k.access 
				if access == 'User':
					return redirect('/order/placeorder')
				else:
					return redirect('/order/listorders')
			else:
				return HttpResponse("Invalid login essentials", {})

	else:
		form = LoginForm()

	return render(request, 'accounts/login.html', {'form': form})

def logout_view(request):
    logout(request)
    return render(request, 'accounts/logout.html')
