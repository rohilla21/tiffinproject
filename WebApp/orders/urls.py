from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.home, name='home'),
	url(r'^placeorder/$', views.placeorder, name='placeorder'),
    url(r'^listorders/$', views.listorders, name='listorders'),
    url(r'^order_history/$', views.order_history, name='order_history'),
]
