from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

class UserProfile(models.Model):
	ACCESS_OPTIONS = (
		('Admin', 'Admin'),
		('User', 'User'),
		)
	user = models.OneToOneField(User)
	access = models.CharField(max_length=10, choices=ACCESS_OPTIONS, default='User')
	contact_number = models.CharField(default=0, max_length=10)
	balance_amount = models.IntegerField(default=0)

	def __str__(self):
		return self.user.username

def create_profile(sender, **kwargs):
	if kwargs['created']:
		user_profile = UserProfile.objects.create(user=kwargs['instance'])

post_save.connect(create_profile, sender=User)