from django.shortcuts import render, redirect, get_object_or_404
from .forms import OrderForm
from .models import Order, AddPrice
from django.utils import timezone
from accounts.models import UserProfile
from django.contrib.auth.models import User
from django.contrib.auth.decorators import user_passes_test

def check_admin(user):
	return user.is_superuser

def home(request):
	return render(request, 'orders/home.html')

def placeorder(request):
	if request.method == 'POST':
		form = OrderForm(request.POST)
		if form.is_valid():
			order = form.save(commit=False)
			order.user = request.user
			order.payable_amount = calculatePayment(order.quantity, order.time)
			updateBalance(request.user, order.payable_amount)
			order = form.save()
			return redirect('/order/listorders')
	else:
		form = OrderForm()
	return render(request, 'orders/placeorder.html', {'form': form})

@user_passes_test(check_admin)
def listorders(request):
	orders = Order.objects.all().order_by('-date')
	return render(request, 'orders/listorders.html', {'orders' : orders})

def calculatePayment(quantity, time):
	if time == 'B':
		k = AddPrice.objects.get(id=1)
		return quantity*k.cost_of_breakfast
	elif time == 'L':
		k = AddPrice.objects.get(id=1)
		return quantity*k.cost_of_lunch
	elif time == 'D':
		k = AddPrice.objects.get(id=1)
		return quantity*k.cost_of_dinner

def updateBalance(user, payment):
	k = UserProfile.objects.get(user=user)
	print(k)
	print (k.balance_amount)
	k.balance_amount -= payment
	k.save()

def order_history(request):
	orders = Order.objects.all().filter(user=request.user).order_by('-date')
	return render(request, 'orders/order_history.html', {'orders' : orders})