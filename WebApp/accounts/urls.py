from django.conf.urls import url
from . import views
#from django.contrib.auth.views import login, logout

urlpatterns = [
	url(r'^login/$', views.login_view, name='login_view'),
	#url(r'^auth/$', views.my_view, name='my_view'),
	url(r'^logout/$', views.logout_view, name='logout_view'),
	url(r'^register/$', views.register, name='register'),
	url(r'^profile/$', views.view_profile, name='view_profile'),
	url(r'^profile/edit/(?P<pk>\d+)/$', views.edit_profile, name='edit_profile'),
	url(r'^allusers/$', views.allusers, name='allusers'),
]

# {'template_name':'accounts/login.html'},
# {'template_name':'accounts/logout.html'}, 