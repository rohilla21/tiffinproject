from django.contrib import admin
from .models import Order, AddPrice

admin.site.register(Order)

admin.site.register(AddPrice)
