from django import forms
from django.forms import ModelForm
from accounts.models import UserProfile
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

class RegistrationForm(UserCreationForm):
	email = forms.EmailField(required=True)

	class Meta:
		model = User
		fields = {'username', 'first_name', 'last_name', 'email', 'password1', 'password2',}

	def save(self, commit=True):
		user = super(RegistrationForm, self).save(commit=False)
		user.first_name = self.cleaned_data['first_name']
		user.last_name = self.cleaned_data['last_name']
		user.email = self.cleaned_data['email']

		if commit:
			user.save()

		return user

class EditProfileForm(UserChangeForm):
	class Meta:
		model = UserProfile
		fields = {'balance_amount', 'password', }

class LoginForm(forms.Form):
	username = forms.CharField(label='username', max_length=100)
	password = forms.CharField(widget=forms.PasswordInput())
	#access = forms.CharField(label='access', max_length=100)