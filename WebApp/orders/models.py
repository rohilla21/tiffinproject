from django.db import models
import datetime
from django.contrib.auth.models import User

class Order(models.Model):
	TIME_OPTIONS = (
		('B', 'Breakfast'),
		('L', 'Lunch'),
		('D', 'Dinner'),
		)
	#STATUS_OPTIONS = (
	#	('O', 'Ordered'),
	#	('D', 'Delievered'),
	#	('C', 'Cancelled'),
	#	)
	user = models.ForeignKey(User)
	date = models.DateField(default=datetime.date.today)
	time = models.CharField(max_length=1, choices=TIME_OPTIONS)
	quantity = models.IntegerField(default=1)
	#status = models.CharField(max_length=1, default='O')	
	payable_amount = models.IntegerField(blank=True)

	def __str__(self):
		return self.id

class AddPrice(models.Model):
	cost_of_breakfast = models.IntegerField(default=45)
	cost_of_lunch = models.IntegerField(default=45)
	cost_of_dinner = models.IntegerField(default=45)

